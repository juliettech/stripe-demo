# Stripe API Demo ⚡️

## What it is
Web application displaying a sellers' top customers, along with additional metrics based on the data coming from the Stripe API.

## Stack
- Front-end: React JS + Bootstrap
- Back-end: Express JS
- Deployment: Currently only works locally 

## How to run the application locally

1. Clone repository

```bash
# In Terminal:
git clone git@gitlab.com:juliettech/stripe-demo.git
```

2. Install packages

```bash
# In Terminal:
cd stripe-demo
cd back-end && npm i
cd ../front-end && npm i
```

3. Make sure you have the environment keys set up. These can be found [here](dashboard.stripe.com) with the logins provided to `me@juliet.tech` by Stripe.

```bash
# In Terminal:
cd back-end && touch .env
# In .env, add this line:
STRIPE_API_SECRET="sk_test_stripe_secret_key"
```

4. Run servers (both need to be active for the platform to work)

```bash
# In Terminal:
cd back-end && npm start
cd ../front-end && npm start
```

5. For testing

```bash
# In Terminal:
npm test
```

## Things to look out for
- You will find this repository filled with comments, since it's meant to be documentation on an implementation. Traditionally however, I like to avoid using comments by writing tests and making sure my code is as expressive and easy to read as possible. You can find more on my philosophy about that [here](https://juliet.tech/clean-code).
- It may take a few seconds to boot up the information. In a production scenario, this would be a priority to fix, along with adding tests, and segmenting the front-end using smaller components. 
- Once deployement is set up, we can also adapt the architecture so calls are being made more securely.
- **Front-end**: Most of the application lives in the `Dashboard` page.
- **Back-end**: Most of the application lives in `index.js`.

## Deployment
Could be done through many different ways, but I would potentially choose AWS Amplify or a custom S3 + EC2 setup, based on budget and time available.

