import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Dashboard from './pages/Dashboard/index';

function App() {
  return <Dashboard />;
}

export default App;
