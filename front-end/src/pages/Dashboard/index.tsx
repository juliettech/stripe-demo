import { useEffect, useState } from 'react';
import axios from 'axios';
import Col from 'react-bootstrap/Col'; // According to Bootstrap documentation, this is the best way to call the components. (https://react-bootstrap.github.io/getting-started/introduction/)
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import { STRINGS } from '../../strings'; // Strings are localized in case we would want to add a new language later.
import { moneyFormat } from '../../helpers';

const headers: Readonly<Record<string, string | boolean>> = {
  "Accept": "application/json",
  "Content-Type": "application/json; charset=utf-8",
  "Cache-Control": "no-cache",
  "Access-Control-Allow-Credentials": true,
  "X-Requested-With": "XMLHttpRequest",
};

// These are stored outside the component since they're not dependant upon any of the functions/data inside it. 
const initialCharge: Charge = {
  id: '',
  customer: '',
  description: '',
  amount: '',
  currency: ''
};

const initialData: Data = {
  sortedCharges: [initialCharge],
  avgOrderValue: 0,
  totalSales: 0,
  sortedProducts: [['', 0]]
};

function renderTableBody(charge: Charge, index: number): JSX.Element {
  const { id, customer, amount, currency, description } = charge;
  
  // Ideally, when a user would click on a row, this would take you to see further information about the customer and transaction. 
  return (
    <tr key={index}> 
      <td>{index + 1}</td>
      <td>{id}</td>
      <td>{customer}</td>
      <td>{`${amount} ${currency.toUpperCase()}`}</td>
      <td>{description}</td>
    </tr>
  )
};

function renderTopProducts(product: (string|number)[]) {
  // I would have pluralized the product name to make it easier for the user to read.
  return <h5 className="card-title">{`${product[0]}: sold ${product[1]} times`}</h5>;
};

interface Charge {
  id: string;
  customer: string;
  amount: string;
  currency: string;
  description: string;
}

interface Data {
  sortedCharges: Charge[];
  avgOrderValue: number;
  totalSales: number;
  sortedProducts: ((string|number)[])[];
}

export default function Dashboard(): JSX.Element {
  const [loading, setLoading] = useState<boolean>(true);
  const [data, setData] = useState<Data | undefined>(initialData);

  useEffect(() => {
    async function getCharges(): Promise<void> {
      // For a production app, I would have a different base url based on the environment using process.env.NODE_ENV.
      
      // Additionally, considering I would likely be doing lots of different calls to the same API, I would study the 
      // possibility of building a Stripe model connected to my backend where I would make all calls to our Stripe API 
      // so it's cleaner in the long run. 
      const { data } = await axios.get('http://localhost:8282/charges', headers); 

      setData(data);
      setLoading(false);
    }

    getCharges();
  }, [])

  // I would usually have separated each section of this page into subcomponents for easier read and pass the info needed through props. 
  // For ex- Metrics.tsx to store both cards, ChargesTable.tsx to store all charges, 
  // LoadingAnimation.tsx in a separate component so it can be shared accross screens in case it's needed in the future, etc. 
  return (
    <Container className="mt-5">
      <Row>
        <h1>{STRINGS.dashboardTitle}</h1>
        <h5>{STRINGS.dashboardDescription}</h5>
      </Row>
      <br />

      {loading && (
        <Row className="d-flex justify-content-between align-items-center mt-5">
          <Col xs={{ span: 3, offset: 3 }}>
            <strong>{STRINGS.loading}</strong>
          </Col>
          <Col xs={{ span: 3, offset: 3 }}>
            <div className="spinner-border ml-auto align-items-center text-success" role="status" aria-hidden="true"></div>
          </Col>
        </Row>
      )}

      {!loading && data &&
        <>
          <Row>
            <Col xs={6}>
              <div className="card text-white bg-success mb-3">
                <div className="card-header">
                  <strong>{STRINGS.avgOrderValue}</strong>
                </div>
                <div className="card-body">
                  <p className="card-text">{`= ${STRINGS.avgOrderValueDescription}`}</p>
                  <h4 className="card-title">{`${moneyFormat(data.avgOrderValue) ?? ''} USD`}</h4>
                </div>
              </div>

              <div className="card text-white bg-success mb-2">
                <div className="card-header">
                  <strong>{STRINGS.totalSales}</strong>
                </div>
                <div className="card-body">
                  <p className="card-text">{`= ${STRINGS.totalSalesDescription}`}</p>
                  <h4 className="card-title">{`${moneyFormat(data.totalSales) ?? ''} USD`}</h4>
                </div>
              </div>
            </Col>

            <Col xs={6}>
              <div className="card text-white bg-success mb-3">
                <div className="card-header">
                  <strong>{STRINGS.bestSeller}</strong>
                </div>
                <div className="card-body">
                  <p className="card-text">{`= ${STRINGS.bestSellerDescription}`}</p>
                  {data?.sortedProducts?.map((product) => renderTopProducts(product))}
                </div>
              </div>
            </Col>
          </Row>
          <br />

          <Row>
            <Table striped bordered responsive size="md" variant="dark">
              <thead>
                <tr>
                  <th>{STRINGS.ranking}</th>
                  <th>{STRINGS.chargeId}</th>
                  <th>{STRINGS.customerEmail}</th>
                  <th>{STRINGS.amountCaptured}</th>
                  <th>{STRINGS.description}</th>
                </tr>
              </thead>

              <tbody>
                {data?.sortedCharges?.map((charge, index) => renderTableBody(charge, index))}
              </tbody>
            </Table>
          </Row>
        </>
      }

      {/* This would appear at the bottom of the page always. */}
      <br /> 
      <Row className="mt-5">
        <p>This was built with 💙 and ✨ by <span><a href="https://juliet.tech" target="_blank" rel="noreferrer">Juliette Chevalier</a></span> for Stripe.</p>
      </Row> 
    </Container>
  )
}