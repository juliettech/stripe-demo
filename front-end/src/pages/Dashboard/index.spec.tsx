// I would have added unit tests checking that the data was fetched properly and that each component's behavior acted as expected.
import { create, ReactTestRenderer } from 'react-test-renderer';
import Dashboard from './';

describe('Dashboard', () => {
  let dashboardComponent: ReactTestRenderer;

  beforeEach(() => {
    dashboardComponent = create(<Dashboard />);
  })

  it('should match snapshot', () => {
    expect(dashboardComponent).toMatchSnapshot();
  });

  it('should return the charges and metrics requested by calling on Stripe API', () => {
    // Mock the return call
    // Make the call
    // Make sure you don't get errors
  })
})