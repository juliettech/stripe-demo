export function moneyFormat(num) {
  const decimals = 2;
  const x = num * Math.pow(10, decimals);
  const r = Math.round(x);
  const br = Math.abs(x) % 1 === 0.5 ? (r % 2 === 0 ? r : r - 1) : r;

  return (br / Math.pow(10, decimals)).toFixed(decimals);
}
