export const STRINGS = {
  amountCaptured: 'Amount Captured',
  avgOrderValue: 'Average Order Value ⚡️',
  avgOrderValueDescription: 'Total Sales / Total Order',
  bestSeller: 'Best Selling Products ⭐️',
  bestSellerDescription: 'These are your best sold products',
  chargeId: 'Charge Id',
  customerEmail: 'Customer Email',
  description: 'Description',
  loading: 'Loading.. This may take a few seconds. Please hold while we fetch your data. 💸',
  metrics: 'Metrics 🤓',
  dashboardTitle: 'Hello 👋🏼 Welcome to your dashboard! 🚀',
  ranking: 'Ranking #',
  dashboardDescription: 'Your hub for metrics on sales and finding out who your top 20 customers are.',
  totalSales: 'Total Sales 💰',
  totalSalesDescription: 'The total amount you\'ve sold'
};