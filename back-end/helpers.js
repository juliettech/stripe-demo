function merge(left, right) {
  const sorted = [];

  while (left.length && right.length) {
    if (left[0].amount >= right[0].amount) { 
      sorted.push(left.shift());
    } else { 
      sorted.push(right.shift());
    } 
  }
  
  let results = [...sorted, ...left, ...right]; 
  
  return results;
}

module.exports = {
  mergeSort: function mergeSort(array) { 
    if (array.length < 2) {
      return array; 
    }
  
    const middle = Math.floor(array.length / 2);
    const left = array.slice(0, middle);
    const right = array.slice(middle);
  
    return merge(mergeSort(left), mergeSort(right));
  },
  count: function countWords(array) {
    return array.reduce(function(objAccumulator, value) {
      objAccumulator[value] = ++objAccumulator[value] || 1;
      return objAccumulator;
    }, {});
  }
}