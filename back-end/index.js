const dotenv = require('dotenv').config();
const cors = require('cors');
const express = require('express');
const stripe = require('stripe')(process.env.STRIPE_API_SECRET);
const { count, mergeSort } = require('./helpers');
   
// Initializes Express
const app = express();

// Using Middleware
// Makes sure Express handles the information so it comes as a JSON.
app.use(express.json());

// Ready for cross-object references
app.use(cors());

// Routes
// I would usually have crafted a Graphql API to make sure I can make less calls,
// in a more strategic way, and keep each resolver isolated. However, due to the
// time constraint and budget, I opted for making a single call that would arrange all info needed.
app.get('/charges', async (_req, res) => {
  const charges = [];
  const productNames = [];

  try {
    for await (const charge of stripe.charges.list({ limit: 100,  expand: ['data.customer'] })) {
      const { id, customer, amount, currency, description } = charge;
      
      // Since there wasn't a productName property and based on the pattern, it seemed reasonable to play 
      // with the assumption that the last word of each description was the closest to the name of the product itself.
      productNames.push(description.split(' ')[2]);

      charges.push({
        id,
        customer: customer.email,
        amount: amount / 100, 
        currency,
        description
      })
    }
    
    // Sorting Charges using merge sort algorithm and keeping only the top 20.
    // Figuring out how many to return was a tough choice. On one end, it's probably beneficial the user can see all of them sorted.
    // On the other end, this made the front-end computation take longer to display all of them.
    // Potential Solutions:
    // - Pagination: Return all of them and have the front-end work a pagination strategy to display the next 20 available.
    // - Webhooks: Listen for events and integrate it with our database to automatically trigger reactions.
    // - Would love to hear how other applications handle this.
    const sortedCharges = mergeSort(charges).slice(0, 20);
    
    // Calculating the Average Order Value: Total Sales / Total Orders
    // Because these amounts may come in different currencies, I would account for this possibility within my production code.
    const amounts = charges.map((charge) => charge.amount);
    const totalSales = amounts.reduce(((acc, amount) => acc + amount), 0);
    const avgOrderValue = totalSales/charges.length;

    // Calculating the best seller categories
    const productsCount = count(productNames);
    const sortedProducts = Object.entries(productsCount).sort((a, b) => b[1] - a[1]).slice(0, 5);

    const response = {
      avgOrderValue,
      sortedCharges,
      totalSales,
      sortedProducts
    }
    
    res.send(response);
  } catch (e) {
    // I would add an Error handler here so each error is presented in an easier way to read when returned.
    res.send(e);
  }
})
  
// Listen
app.listen(8282, () => console.log('listening at port 8282'));
